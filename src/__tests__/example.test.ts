import { createConnection, Connection } from "typeorm";
import moment from "moment";
import env from "dotenv";
import { } from "jest";

let connection: Connection;

beforeAll(async () => {
	env.config();

	connection = await createConnection();

	if (!connection) {
		throw new Error("Error Connection To Database");
	}

});

afterAll(async () => {
	// await connection!.close();
});

describe.skip("DAL", () => {
	test("test Connection", async () => {
		const isConnected = await connection.isConnected();
		expect(isConnected).toBe(true);
	});
});
